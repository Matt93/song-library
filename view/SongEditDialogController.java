/**
 * Assignment 1: Song Library
 * 
 * Matthew Deleon & Alex Chung
 * CS 213 Software Methodology, Spring 2016
 * 
 */

package view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Song;

/**
 * Dialog to edit details of a song.
 */
public class SongEditDialogController {

    @FXML
    private TextField songNameField;
    @FXML
    private TextField artistField;
    @FXML
    private TextField yearField;
    @FXML
    private TextField albumField;


    private Stage dialogStage;
    private Song song;
    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     * 
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the song to be edited in the dialog.
     * 
     * @param song
     */
    public void setSong(Song song) {
        this.song = song;

        songNameField.setText(song.getSongName());
        artistField.setText(song.getArtist());
        yearField.setText(song.getYear());
        albumField.setText(song.getAlbum());
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     * 
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks OK.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            song.setSongName(songNameField.getText());
            song.setArtist(artistField.getText());
            song.setAlbum(albumField.getText());
            song.setYear(yearField.getText());

            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     * 
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (songNameField.getText() == null || songNameField.getText().length() == 0) {
            errorMessage += "No valid song name!\n"; 
        }
        
        if (artistField.getText() == null || artistField.getText().length() == 0) {
            errorMessage += "No valid artist!\n"; 
        }
        if (albumField.getText() == null || albumField.getText().length() == 0) {
        	albumField.setText("(none)"); 
        }
        if (yearField.getText() == null || yearField.getText().length() == 0) {
        	yearField.setText("(none)");	
        }
        
        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
}
