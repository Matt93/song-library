/**
 * Assignment 1: Song Library
 * 
 * Matthew Deleon & Alex Chung
 * CS 213 Software Methodology, Spring 2016
 * 
 */

package model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Helper class to wrap a list of songs. This is used for saving the
 * list of songs to XML.
 */
@XmlRootElement(name = "songs")
public class SongLibWrapper {

    private List<Song> songs;

    @XmlElement(name = "song")
    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }
}
