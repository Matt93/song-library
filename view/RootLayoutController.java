/**
 * Assignment 1: Song Library
 * 
 * Matthew Deleon & Alex Chung
 * CS 213 Software Methodology, Spring 2016
 * 
 */

package view;

import java.io.File;
import java.util.Optional;

import app.SongLib;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;

/**
 * The controller for the root layout. The root layout provides the basic
 * application layout containing a menu bar and space where other JavaFX
 * elements can be placed.
 */
public class RootLayoutController {

    // Reference to the main application
    private SongLib mainApp;

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(SongLib mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * Creates an empty song library.
     */
    @FXML
    private void handleNew() {
        mainApp.getSongData().clear();
        mainApp.setSongFilePath(null);
    }

    /**
     * Opens a FileChooser to let the user select a song library to load.
     */
    @FXML
    private void handleOpen() {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show save file dialog
        File file = fileChooser.showOpenDialog(mainApp.getPrimaryStage());

        if (file != null) {
            mainApp.loadSongDataFromFile(file);
        }
    }

    /**
     * Saves the file to the song file that is currently open. If there is no
     * open file, the "save as" dialog is shown.
     */
    @FXML
    private void handleSave() {
        File songFile = mainApp.getSongFilePath();
        if (songFile != null) {
            mainApp.saveSongDataToFile(songFile);
        } else {
            handleSaveAs();
        }
    }

    /**
     * Opens a FileChooser to let the user select a file to save to.
     */
    @FXML
    private void handleSaveAs() {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show save file dialog
        File file = fileChooser.showSaveDialog(mainApp.getPrimaryStage());

        if (file != null) {
            // Make sure it has the correct extension
            if (!file.getPath().endsWith(".xml")) {
                file = new File(file.getPath() + ".xml");
            }
            mainApp.saveSongDataToFile(file);
        }
    }

    /**
     * Opens an about dialog.
     */
    @FXML
    private void handleAbout() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Song Library");
        alert.setHeaderText("About");
        alert.setContentText("Created by: Matthew Deleon & Alex Chung\n		   Software Methodology, Spring 2016");

        alert.showAndWait();
    }

    /**
     * Closes the application.
     */
    @FXML
    private void handleExit() {
    	/*
         * Opens a dialog asking the user if he or she has saved their song list.
         * If yes, the program terminates. 
         * If no, the dialog terminates and the program resumes.
         */
    	Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Make sure to save your progress before exiting!");
		alert.setHeaderText("To save, click File -> Save, then\nchoose where you would like to save.");
		alert.setContentText("Have you saved?");
		
		ButtonType btnYes = new ButtonType("Yes");
		ButtonType btnNo = new ButtonType("No");
		ButtonType btnNoExitAnyway = new ButtonType("No, exit anyway");
		
		alert.getButtonTypes().setAll(btnYes, btnNo, btnNoExitAnyway);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == btnYes || result.get() == btnNoExitAnyway)
		    System.exit(0);   
    }
}
