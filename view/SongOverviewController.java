/**
 * Assignment 1: Song Library
 * 
 * Matthew Deleon & Alex Chung
 * CS 213 Software Methodology, Spring 2016
 * 
 */

package view;

import app.SongLib;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import model.Song;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class SongOverviewController {
    @FXML
    private TableView<Song> songTable;
    @FXML
    private TableColumn<Song, String> songColumn;
    @FXML
    private Label songNameLabel;
    @FXML
    private Label artistLabel;
    @FXML
     private Label albumLabel;
    @FXML
    private Label yearLabel;

    // Reference to the main application.
    private SongLib mainApp;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public SongOverviewController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    	songTable.getSelectionModel().selectFirst();
        // Initialize the song table with the one column.
        songColumn.setCellValueFactory(
                cellData -> cellData.getValue().songNameProperty());
        // Listen for selection changes and show the song details when changed.
        songTable.getSelectionModel().selectedItemProperty().addListener(
               (observable, oldValue, newValue) -> showSongDetails(newValue));
        songTable.getSelectionModel().selectFirst();
    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(SongLib mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        songTable.setItems(mainApp.getSongData());
    }
    
    /**
 	* 	Called when user clicks OK to add or edit a song. Alerts the user 
 	*  that the song already exists in their list.
 	*/
	private boolean checkExists(Song s){
		ObservableList <Song> song = mainApp.getSongData();
		for(Song o: song){
			if(s.getSongName().equals(o.getSongName())){
				return true;
			}
		}
		return false;
	}

    /**
     * Fills all text fields to show details about the song.
     * If the specified song is null, all text fields are cleared.
     *
     * @param song the song or null
     */
   // @SuppressWarnings("unused")
	private void showSongDetails(Song song) {
        if (song != null) {
            // Fill the labels with info from the song object.
			songNameLabel.setText(song.getSongName());
            artistLabel.setText(song.getArtist());
            albumLabel.setText(song.getAlbum());
            yearLabel.setText(song.getYear());
        } else {
            // Song is null, remove all the text.
            songNameLabel.setText("");
            artistLabel.setText("");
            albumLabel.setText("");
            yearLabel.setText("");
        }
    }

	/**
	 * Called when the user clicks on the delete button.
	 */
	@FXML
	private void handleDeleteSong() {
	    int selectedIndex = songTable.getSelectionModel().getSelectedIndex();
	    if (selectedIndex >= 0) {
	        songTable.getItems().remove(selectedIndex);
	    } else {
	        // Nothing selected.
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("No Selection");
	        alert.setHeaderText("No song selected.");
	        alert.setContentText("Please select a song in the table.");

	        alert.showAndWait();
	    }
	}

	

	/**
	 * Called when the user clicks the new button. Opens a dialog to edit
	 * details for a new song.
	 */
	@FXML
	private void handleNewSong() {
	    Song tempSong = new Song();
	    boolean okClicked = mainApp.showSongEditDialog(tempSong);
	    	if (okClicked) {
	    			if(!checkExists(tempSong)){
	    				mainApp.getSongData().add(tempSong);
	    			}
	    			else {
	    				Alert alert = new Alert(AlertType.ERROR);
	    				 	alert.initOwner(mainApp.getPrimaryStage());
	    			        alert.setTitle("Error");
	    			        alert.setHeaderText("Song already exists");
	    			        alert.setContentText("Please add another song.");
	    			        alert.showAndWait();
	    			}
	    		}
	    }


	/**
	 * Called when the user clicks the edit button. Opens a dialog to edit
	 * details for the selected song.
	 */
	@FXML
	private void handleEditsong() {
	    Song selectedSong = songTable.getSelectionModel().getSelectedItem();
	    if (selectedSong != null) {
	        boolean okClicked = mainApp.showSongEditDialog(selectedSong);
	        if (okClicked) {
	        	if(!checkExists(selectedSong)){
    				showSongDetails(selectedSong);
    			}
    			else {
    				Alert alert = new Alert(AlertType.ERROR);
    				 	alert.initOwner(mainApp.getPrimaryStage());
    			        alert.setTitle("Error");
    			        alert.setHeaderText("Song already exists");
    			        alert.setContentText("Please add another song.");
    			        alert.showAndWait();  			   
    			}
	        }
	    } else {
	        // Nothing selected.
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("No Selection");
	        alert.setHeaderText("No song selected");
	        alert.setContentText("Please select a song in the table.");

	        alert.showAndWait();
	    }
	}
}