/**
 * Assignment 1: Song Library
 * 
 * Matthew Deleon & Alex Chung
 * CS 213 Software Methodology, Spring 2016
 * 
 */

package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Model class for a Song.
 */
public class Song {

    private final StringProperty songName;
    private final StringProperty artist;
    private final StringProperty album;
    private final StringProperty year;

    /**
     * Default constructor.
     */
    public Song() {
        this(null);
    }

    /**
     * Constructor with some initial data.
     *
     * @param songName
     *
     */
    public Song(String songName) {
        this.songName = new SimpleStringProperty(songName);
        this.artist = new SimpleStringProperty("some artist");
        this.album = new SimpleStringProperty("some album");
        this.year = new SimpleStringProperty("some year");
    }

    public String getSongName() {
        return songName.get();
        
    }

    public void setSongName(String songName) {
        this.songName.set(songName);
    }

    public StringProperty songNameProperty() {
        return songName;
    }

    public String getArtist() {
        return artist.get();
    }

    public void setArtist(String artist) {
        this.artist.set(artist);
    }

    public StringProperty artistProperty() {
        return artist;
    }
    public String getAlbum() {
        return album.get();
    }

    public void setAlbum(String album) {
        this.album.set(album);
    }

    public StringProperty albumProperty() {
        return album;
    }
    
    public String getYear() {
        return year.get();
    }

    public void setYear(String year) {
        this.year.set(year);
    }

    public StringProperty yearProperty() {
        return year;
    }

}